# Pipeline para Temas do dothnews e do Classificados

## Instalação do Pipeline no Tema

### 1. Ativar o pipeline 

Criar um arquivo chamado bitbucket-pipelines.yml e colocar na raiz do tema com o mesmo o conteúdo do arquivo sample-bitbucket-pipelines.yml   

### 2. Configurar variável do repositório do cliente 

Configurar a variável CLIENTE_USER com o domínio do cliente. 
https://drive.google.com/file/d/1OAZh13crCHNFdI9ZmXyNmdtS1ZIhjKi1/view?usp=sharing

### 3. Arquivo bitbucket-pipelines.yml

Criar um arquivo chamado bitbucket-pipelines.yml e colocar na raiz do tema com o mesmo o conteúdo do arquivo sample-bitbucket-pipelines.yml            

## Manutenção de pipeline (Só faça se realmente souber o que está fazendo)

### 1. Executando localmente .deploy_theme.sh 

Entrar na raiz do diretorio do tema e executar os comandos abaixo

$git clone git@bitbucket.org:dothcom/pipeline-de-tema.git pipeline

$sh pipeline/.deploy_theme.sh onovaes vejaobem.com.br




