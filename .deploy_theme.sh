#!/bin/bash
#
# Version: 0.4
#
##### VARIAVEIS DE CONTA (BITBUCKET)
# https://bitbucket.org/account/user/%7Be0d59186-0945-4335-9845-ae51666c8cb8%7D/addon/admin/pipelines/account-variables 
# $1 - $DEPLOYER_USER - nome do usuário que com permissão para fazer deploy nos app's servers
#
##### VARIAVEIS DO TEMA (Configuracao do tema) 
# $2 - $CLIENTE_USER 		- usuario do cliente(news) na LB, ex vejaobem.com.br


################### APENAS PARA TESTES, EM AMBIENTE ONDE NAO TEMOS AS VARIVAEIS DE CONTA
# DEPLOY_HOST_APP01=145.79.11.6
# DEPLOY_HOST_APP02=145.79.9.69


echo "Chamado com $# parametro(s)"
DEPLOYER_USER=$1
CLIENTE_USER=$2

# Set high expectations so that Pipelines fails on an error or exit 1
# Util para parar o pipeline quando falhar
set -e

# https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-remote_host
# Pega o DSA do host - 
# Basicamente é pra evitar de ficar colocando manualmente cada Known hosts nas configurações do tema - https://drive.google.com/file/d/1zGkww5i-ddvWZEQNbReWkJzPlMYtd_xV/view?usp=sharing
get_dsa_host_key()
{
	ssh-keyscan -t rsa $2 > my_known_hosts
	mkdir -p ~/.ssh
	cat my_known_hosts >> ~/.ssh/known_hosts
	(umask  077 ; echo $DEPLOYER_USER_KEY | base64 --decode > ~/.ssh/id_rsa)
	rm my_known_hosts
}


# Cria pasta staging_themes caso não exista
create_staging_dir(){
	echo "Criando a pasta de staging em '$2'"
	ssh $1@$2 "mkdir -p staging_themes"	
}



checa_variaveis_existem(){
	#Validando se variaveis existem https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
	if [ -z ${DEPLOYER_USER} ]; then echo "DEPLOYER_USER is unset"; exit 1; else echo "DEPLOYER_USER is set to '$DEPLOYER_USER'"; fi
	if [ -z ${CLIENTE_USER} ]; then echo "Variavel CLIENTE_USER nao esta setada"; exit 1; else echo "CLIENTE_USER is set to '$CLIENTE_USER'"; fi
}

local_to_staging()
{
	echo "INICIO DO RSYNC do tema LOCAL para a pasta /home/deployer/staging_themes/ em $2 usuário $CLIENTE_USER"	
	git describe --all --long | cut -d "-" -f 3 > version.txt

	rsync 	-azrh \
			--stats \
			--delete-excluded \
			--exclude '_html*' \
			--exclude 'pipeline' \
			--exclude 'bitbucket-pipelines.yml' \
			--exclude '/.*' \
			. $1@$2:staging_themes/$3

	rm version.txt
}

staging_to_themedir()
{
	ssh $1@$2 "sudo rsync \
				-azrh \
				--stats \
				--delete-excluded \
				staging_themes/$3 /home/$3/public_html/application/themes/"
}

copy_cliente_routes()
{
	file="/home/$3/public_html/application/themes/$3/cliente_routes.php"
	dest_file="/home/$3/public_html/application/config/local_routes.php"

	if ssh $1@$2 [[ -f "$file" ]]	
	then
 		echo "Copiando $file para dest_file$"
 		ssh $1@$2 "sudo cp -f $file $dest_file"
 		#CORRIGE PERMISSAO
 		ssh $1@$2 "sudo chown  $3:$3 $dest_file "
 	else
		echo "Cliente SEM arquivo $file"
	fi
}

#copiar o manifest.json da pasta do cliente para  raiz do site
copy_pwa_manifest_json()
{
	file="/home/$3/public_html/application/themes/$3/pwa/manifest.json"
	dest_file="/home/$3/public_html/manifest.json"

	if ssh $1@$2 [[ -f "$file" ]]	
	then
 		echo "Copiando $file para dest_file$"
 		ssh $1@$2 "sudo cp -f $file $dest_file"
 		#CORRIGE PERMISSAO
 		ssh $1@$2 "sudo chown  $3:$3 $dest_file "
 	else
		echo "Cliente SEM arquivo $file"
	fi
}

#copiar favixon.ico da pasta do cliente para  raiz do site
copy_favicon()
{
	file="/home/$3/public_html/application/themes/$3/gfx/favicon/favicon.ico"
	dest_file="/home/$3/public_html/favicon.ico"

	if ssh $1@$2 [[ -f "$file" ]]	
	then
 		echo "Copiando $file para dest_file$"
 		ssh $1@$2 "sudo cp -f $file $dest_file"
 		ssh $1@$2 "sudo chown  $3:$3 $dest_file "
 	else
		echo "Cliente SEM arquivo $file"
	fi
}

#copiar o sw.js da pasta do cliente para  raiz do site
copy_pwa_swjs()
{
	file="/home/$3/public_html/application/themes/$3/pwa/sw.js"
	dest_file="/home/$3/public_html/sw.js"

	if ssh $1@$2 [[ -f "$file" ]]	
	then
 		echo "Copiando $file para dest_file$"
 		ssh $1@$2 "sudo cp -f $file $dest_file"
 		#CORRIGE PERMISSAO
 		ssh $1@$2 "sudo chown  $3:$3 $dest_file "
 	else
		echo "Cliente SEM arquivo $file"
	fi
}

copy_cliente_htaccess()
{
	file="/home/$3/public_html/application/themes/$3/htaccess_personalizado/.htaccess"
	dest_file="/home/$3/public_html/.htaccess"

	if ssh $1@$2 [[ -f "$file" ]]	
	then
 		echo "Copiando $file para dest_file$"
 		ssh $1@$2 "sudo cp -f $file $dest_file"
 		#CORRIGE PERMISSAO
 		ssh $1@$2 "sudo chown  $3:$3 $dest_file "
 	else
		echo "Cliente SEM arquivo $file"
	fi
}


seta_dono_permissao()
{
	echo "Seta Grupo/Usuario para '$3'"
	ssh $1@$2 "sudo chown -R $3:$3 /home/$3/public_html/application/themes/$3/ "

	echo '755 to directories'
	ssh $1@$2 "find /home/$3/public_html/application/themes/$3/ -type d -print0 | sudo xargs -0 sudo chmod 0755 "

	echo '644 to files'
	ssh $1@$2 "find /home/$3/public_html/application/themes/$3/ -type f -print0 | sudo xargs -0 chmod 0644"
}



#
# While Enquanto existir variváveis do tipo  DEPLOY_HOST_APP0X
#
i=1
while true; do
	var_name="DEPLOY_HOST_APP0${i}"
	deploy_host=$(eval echo \$${var_name})
	
	if [ -z "$deploy_host" ]; then break; fi

	echo '== INICIO: '${var_name}
	echo $deploy_host
	i=$(( $i + 1 ))

	checa_variaveis_existem
	get_dsa_host_key "$DEPLOYER_USER" "$deploy_host"
	create_staging_dir "$DEPLOYER_USER" "$deploy_host"
	local_to_staging "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER"
	staging_to_themedir "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER"
	copy_cliente_routes "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER"
	copy_pwa_manifest_json "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER"
	copy_pwa_swjs "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER"
	copy_cliente_htaccess "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER"
	copy_favicon "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER"
	seta_dono_permissao "$DEPLOYER_USER" "$deploy_host" "$CLIENTE_USER" 

  	echo '------ FINALIZOU deploy '$DEPLOYER_USER' em '$deploy_host
done
